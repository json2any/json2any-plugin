Change History Log
==================
Note: below this line changes are maintained by `bump2version` tool.
* Add your changes to section below
* The tool will automatically add section for next release 

Pending changes
---------------

*

Release v1.0.0
---------------
* Initial Implementation
  * AbstractArgParser
  * AbstractDataProvider
  * AbstractHelperProvider
  * AbstractTemplateProvider
