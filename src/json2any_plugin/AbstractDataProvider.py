from abc import abstractmethod
from typing import Any, Dict

from json2any_plugin.AbstractArgParser import AbstractArgParser

# The following keys are used by json2any infrastructure
DATA_KEY_QUERY = "query"
DATA_KEY_RUN = "run"
DATA_KEY_RUNS = "runs"
FORBIDDEN_KEYS = [DATA_KEY_QUERY, DATA_KEY_RUN, DATA_KEY_RUNS]


class AbstractDataProvider(AbstractArgParser):

    @abstractmethod
    def load_data(self) -> Dict[str, Any]:
        """
        loads the data for template.
        Do not use the "FORBIDDEN_KEYS"
        :return Dict[str, Any]: the data for template. This dictionary will be "mounted" at "root" in template,
         along with the "DATA_KEY_XXX" data. Do not use the "FORBIDDEN_KEYS" as keys in returned data.
         i.e. if you return {"some_key": "Key Data"} you will access it in template {{ some_key }}
        """
        raise NotImplementedError()
